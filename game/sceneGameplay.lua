SceneGameplay = {}
function SceneGameplay:new()
	local o = {}
	setmetatable(o, self)
	self.__index = self
	-- call to setup the scene 
	self:init()
	return o
end 


-- TODO: add in imgui for ui
-- Collision detection: http://buildnewgames.com/broad-phase-collision-detection/

-- set set of tilesets
local tileSetImages = {
	platformer = love.graphics.newImage("assets/arcade_platformerV2_128px.png")
}

local animationFrameTime = 0.035


function SceneGameplay:init()

	-- function to setup ecs in ecsInit.lua
	startupECS()

	local player = createEntity(componentNames.transform, componentNames.velocity, 
		componentNames.boxCollider, componentNames.animatedSprite, componentNames.groundedFlag,
		componentNames.inputFlag)
	
	local boxColliderComponent = getComponent(player, componentNames.boxCollider)
	boxColliderComponent.extents.yOffs = 64
	boxColliderComponent.extents.xOffs = 32
	boxColliderComponent.extents.width = 64
	boxColliderComponent.extents.height = 64

	local animatedSpriteComponent = getComponent(player, componentNames.animatedSprite)
	animatedSpriteComponent.image = tileSetImages.platformer
	animatedSpriteComponent.timer = Timer:new(animationFrameTime, TimerModes.repeating)
	local imgSize = 128
	animatedSpriteComponent.quadList = {}
	animatedSpriteComponent.quadList[1] = love.graphics.newQuad(0, 			0,			imgSize,	imgSize,	animatedSpriteComponent.image:getDimensions())
	animatedSpriteComponent.quadList[2] = love.graphics.newQuad(imgSize,	0,			imgSize,	imgSize,	animatedSpriteComponent.image:getDimensions())
    animatedSpriteComponent.quadList[3] = love.graphics.newQuad(imgSize*2,	0,			imgSize,	imgSize,	animatedSpriteComponent.image:getDimensions())
    animatedSpriteComponent.quadList[4] = love.graphics.newQuad(0,			imgSize,	imgSize,	imgSize,	animatedSpriteComponent.image:getDimensions())
	animatedSpriteComponent.maxIndex = 4

	local floor = createEntity(componentNames.transform, componentNames.boxCollider)
	
	local floorBoxColliderComponent = getComponent(floor, componentNames.boxCollider)
	floorBoxColliderComponent.extents.width = 500
	floorBoxColliderComponent.extents.height = 100

	local floorPosComponent = getComponent(floor, componentNames.transform)
	floorPosComponent.pos.y = 500



	local floor2 = createEntity(componentNames.transform, componentNames.boxCollider)
	
	local floorBoxColliderComponent2 = getComponent(floor2, componentNames.boxCollider)
	floorBoxColliderComponent2.extents.width = 500
	floorBoxColliderComponent2.extents.height = 100

	local floorPosComponent2 = getComponent(floor2, componentNames.transform)
	floorPosComponent2.pos.x = 600
	floorPosComponent2.pos.y = 500
end 

function SceneGameplay:update(dt)
	if getKeyDown("escape") then 
		love.event.quit()
	end 

	-- udpate velocity and position
	processMovingEntities(dt)
	-- update collisions
	processCollisions(dt)
end 


function SceneGameplay:draw()
	-- non sprite entities / rects (delete)
	local movingEntities = getEntitiesForMask(componentNames.transform, componentNames.boxCollider)
	for i=1,#movingEntities do
		local positionComponent = getComponent(movingEntities[i], componentNames.transform)
		local boxColliderComponent = getComponent(movingEntities[i], componentNames.boxCollider)

		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.rectangle("line", 
			positionComponent.pos.x + boxColliderComponent.extents.xOffs,
		 	positionComponent.pos.y + boxColliderComponent.extents.yOffs, 
			boxColliderComponent.extents.width, boxColliderComponent.extents.height)
	end

	drawAnimatedSprites()

end

