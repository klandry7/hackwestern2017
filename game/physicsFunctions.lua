
function newVector(_x, _y) 
	return {x = _x, y = _y}
end 

function newAABB(_x, _y, _w, _h) 
	return {
		x = _x,
		y = _y,
		width = _w,
		height = _h
	}	
end

function closestPointOnBoundsToPoint(aabb, point)
	local maxX = aabb.x + aabb.width
	local maxY = aabb.y + aabb.height

	local minDist = math.abs(point.x - aabb.x)
	local boundsPoint = newVector(aabb.x, point.y)

	if math.abs(maxX - point.x) < minDist then 
		minDist = math.abs(maxX - point.x)
		boundsPoint = newVector(maxX, point.y)
	end 
	if math.abs(maxY - point.y) < minDist then 
		minDist = math.abs(maxY - point.y) 
		boundsPoint = newVector(point.x, maxY)
	end
	if math.abs(aabb.y - point.y) < minDist then 
		minDist = math.abs(aabb.y - point.y)
		boundsPoint = newVector(point.x, aabb.y)
	end

	return boundsPoint
end

-- entity 1 (moving), entity 2 (static)
-- return AABB
function minkowskiDifference(AABB1e, AABB2e) 

	local pos1 = getComponent(AABB1e, componentNames.transform)
	local box1 = getComponent(AABB1e, componentNames.boxCollider)

	local pos2 = getComponent(AABB2e, componentNames.transform)
	local box2 = getComponent(AABB2e, componentNames.boxCollider)

	local AABB1 = newAABB(pos1.pos.x + box1.extents.xOffs, pos1.pos.y + box1.extents.yOffs, 
		box1.extents.width, box1.extents.height) 

	local AABB2 = newAABB(pos2.pos.x + box2.extents.xOffs, pos2.pos.y + box2.extents.yOffs, 
		box2.extents.width, box2.extents.height)

	local x = AABB1.x - (AABB2.x + AABB2.width)
	local y = AABB1.y - (AABB2.y + AABB2.height)

	local width = AABB1.width + AABB2.width 
	local height = AABB1.height + AABB2.height

	return newAABB(x, y, width, height)
end

