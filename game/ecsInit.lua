
componentNames = {
    transform = "transform", velocity = "velocity", 
    boxCollider = "boxCollider", animatedSprite = "animatedSprite",
    groundedFlag = "groundedFlag", inputFlag = "inputFlag"
}


function startupECS()

	initSystem(1000) 

	addComponentToSystem(
		componentNames.transform, 
		function() return {pos = {x = 0, y = 0}} end, 
		function(component) component.pos.x = 0 component.pos.y = 0 end)

	addComponentToSystem(
		componentNames.velocity, 
		function() return {vel = {x = 0, y = 0}} end, 
		function(component) component.vel.x = 0 component.vel.y = 0 end)

	addComponentToSystem(
		componentNames.boxCollider, 
		function() return {extents = {xOffs = 0, yOffs = 0, width = 0, height = 0}} end, 
		function(component) component.extents.width = 0 component.extents.height = 0 
            component.extents.xOffs = 0 component.extents.yOffs = 0 end)

    addComponentToSystem(
		componentNames.animatedSprite, 
		function() return {image = nil, frame = 1, timer = nil, currentIndex = 1, maxIndex = 1, quadList = {}} end, 
		function(component) component.image = nil component.frame = 1 component.timer = nil 
            component.currentIndex = 1 component.maxIndex = 1 component.quadList = {} end)

    addComponentToSystem(
		componentNames.groundedFlag, 
		function() return {isGrounded = false} end, 
		function(component) component.isGrounded = false end)

	addComponentToSystem(
		componentNames.inputFlag, 
		function() return {} end, 
		function(component) end)

end