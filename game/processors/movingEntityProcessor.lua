
local jumpVelocity = -700
local forwardMoveSpeed = 800.0

function processMovingEntities(dt)
local movingEntities = getEntitiesForMask(componentNames.transform, componentNames.velocity, componentNames.boxCollider)
	for i=1,#movingEntities do
		local positionComponent = getComponent(movingEntities[i], componentNames.transform)
		local velocityComponent = getComponent(movingEntities[i], componentNames.velocity)
		local boxColliderComponent = getComponent(movingEntities[i], componentNames.boxCollider)

		-- keep moving forwards
		velocityComponent.vel.x = forwardMoveSpeed * dt

		-- if you have an input flag
		if hasComponent(movingEntities[i], componentNames.inputFlag) then 
			-- try to jump
			if getKeyPress("w") then
				-- check if you're grounded 
				local groundedFlag = getComponent(movingEntities[i], componentNames.groundedFlag)
				if groundedFlag ~= nil and groundedFlag.isGrounded == true then 
					groundedFlag.isGrounded = false
					velocityComponent.vel.y = jumpVelocity * dt
				end	
			end
		end 
		
		-- add downwards gravity 
		velocityComponent.vel.y = velocityComponent.vel.y + 50 * dt
		-- update position
		positionComponent.pos.y = positionComponent.pos.y + velocityComponent.vel.y
		positionComponent.pos.x = positionComponent.pos.x + velocityComponent.vel.x
	end


end