

-- TODO: separate into update and render
function drawAnimatedSprites()
    -- draw sprites 
	local spriteEntities = getEntitiesForMask(componentNames.transform, componentNames.boxCollider, componentNames.animatedSprite)
	for i=1,#spriteEntities do
		local positionComponent = getComponent(spriteEntities[i], componentNames.transform)
		local boxColliderComponent = getComponent(spriteEntities[i], componentNames.boxCollider)
		local animatedSpriteComponent = getComponent(spriteEntities[i], componentNames.animatedSprite)
		
		-- if timer completes, update animation
		if animatedSpriteComponent.timer:isComplete(0.016) then 
			animatedSpriteComponent.currentIndex = animatedSpriteComponent.currentIndex + 1
			if animatedSpriteComponent.currentIndex > animatedSpriteComponent.maxIndex then animatedSpriteComponent.currentIndex = 1 end
		end 
		
		-- draw sprite
		love.graphics.draw(animatedSpriteComponent.image, 
			animatedSpriteComponent.quadList[animatedSpriteComponent.currentIndex], 
			positionComponent.pos.x, positionComponent.pos.y)
	end
end 