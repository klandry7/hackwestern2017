
function processCollisions(dt)

-- physics update of static staticEntities vs moving entities 
	local movingEntities = getEntitiesForMask(componentNames.transform, componentNames.velocity, componentNames.boxCollider)
	local staticEntities = getEntitiesForMask(componentNames.transform, componentNames.boxCollider)
	for i=1,#staticEntities do	
		local obstacleEntity = staticEntities[i]

		for n=1,#movingEntities do				
			local movingEntity = movingEntities[n]

			-- don't check against yourself (Duh)
			if obstacleEntity ~= movingEntity then 
				local AABBResult = minkowskiDifference(movingEntity, obstacleEntity)
				local collision = false

				-- check for aabb collision
				if  AABBResult.x <= 0 and AABBResult.x + AABBResult.width >= 0 and 
					AABBResult.y <= 0 and AABBResult.y + AABBResult.height >= 0 then 
					collision = true
				end
				-- if there was a collision
				if collision then 				
					-- get penetration vector from collection 
					local penetrationVector = closestPointOnBoundsToPoint(AABBResult, newVector(0, 0))

					-- if you collided with something below you
					if penetrationVector.y > 0 then 
						-- reset velocity if you collided 
						local velocityComponent = getComponent(movingEntity, componentNames.velocity)
						velocityComponent.vel.y = 0

						-- ground yourself
						local groundedFlag = getComponent(movingEntity, componentNames.groundedFlag)
						if groundedFlag ~= nil and groundedFlag.isGrounded == false then 
							groundedFlag.isGrounded = true
						end	
					end		

					-- update position against penetration vector
					local positionComponent = getComponent(movingEntity, componentNames.transform)
					positionComponent.pos.x = positionComponent.pos.x - penetrationVector.x 
					positionComponent.pos.y = positionComponent.pos.y - penetrationVector.y
				end
			end 
		end
	end

end