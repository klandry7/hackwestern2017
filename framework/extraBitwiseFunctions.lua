-- unsigned only (maybe)
-- returns a table of bits, most significant first. (with padding)
-- ie: will print 13 as 1101
-- https://stackoverflow.com/questions/9079853/lua-print-integer-as-a-binary
function bit.bmstable(num, bits)
	-- m,n = math.frexp(v) breaks down a floating point value (v) into mantissa (m) and exponent (n)
	-- the absolute value of m is >= 0.5 and less than 1.0
	-- select the 2nd return value
	-- the number of padding bits is 1 or the exponent value ()
    bits = bits or math.max(1, select(2, math.frexp(num)))
    local t = {} -- will contain the bits        
    for b = bits, 1, -1 do
    	-- the number % 2 will be 0 or 1
        t[b] = math.fmod(num, 2)
        -- floor(number - (1 or 0) / 2)
        -- halve the number while making sure it stays even
        num = math.floor((num - t[b]) / 2)
    end
    return t
end

-- print the given number as binary
function bit.bprint(num, bits)
	local bitTable = bit.bmstable(num, bits)
    -- TODO: format this to print in groups of 4 (add a space every 4)
	print(table.concat(bitTable))
end