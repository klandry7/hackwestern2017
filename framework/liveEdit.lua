LiveEdit = {}
function LiveEdit:new(fileName)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	if not string.find(fileName, ".lua") then 
		fileName = fileName..".lua"
	end 
	o.fileName = fileName
	o.lastModified = love.filesystem.getLastModified(fileName)
	return o
end

function LiveEdit:update()
	------------To make the code update in real time
	if(love.filesystem.getLastModified(self.fileName) ~= self.lastModified)then 
		local testFunc = function()
			love.filesystem.load(self.fileName)
		end
		local test,e = pcall(testFunc)
		if(test)then 
		 	love.filesystem.load(self.fileName)()
		 	love.run()
		else 
			print(e)
		end
		self.lastModified = love.filesystem.getLastModified(self.fileName)
	end
end