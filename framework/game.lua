-- global scene stack
sceneStack = nil

function loadGame()
	sceneStack = Stack:new()

	local sceneA = SceneGameplay:new()
	sceneStack:push(sceneA)

end

function updateGame(dt)

	-- quit event won't call immediately, need safeguards for now
	if sceneStack:peek() == nil then 
		love.event.quit(0)
	else 
		sceneStack:peek():update(dt)
	end 
end

function drawGame()
	if sceneStack:peek() ~= nil then 
		sceneStack:peek():draw()
	end 
end
