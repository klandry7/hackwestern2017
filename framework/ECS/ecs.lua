
-- value indicating no entity 
-- this will mean entityMasks[ENTITY_NONE] is technically valid, but it's a garbage value
-- you can perform ops on it, but it goes to this garbage index
local ENTITY_NONE = -1
 
-- internal max entity count 
local maxEntityCount = -1

-- the current bitmask lshift value
local currentMaskShift = 0
-- list of component mask values 
local masks = { 
COMPONENT_NONE = 0,
-- the rest of the component masks will be dynamically filled
}

-- TODO: define a system max size
local system = {
	-- entity masks is a list of every component mask  
	-- an entity is an index into this list 
	-- the entity index matches the component index
	entityMasks = {},
	-- the rest of the components will be dynamically filled
}

function initSystem(_maxEntityCount) 
	assert(_maxEntityCount > 0, "max entity count must exceed 0")
	assert(maxEntityCount == -1, "you can't initialize the ECS twice")
	maxEntityCount = _maxEntityCount
	-- init everything 
	for i=1,maxEntityCount do
		system.entityMasks[i] = masks.COMPONENT_NONE
	end
end


--in: the string name of the component
function addComponentToSystem(componentName, createComponentFunction, resetComponentFunction)
	-- reserved name messages
	assert(componentName ~= "entityMasks", "component name cannot be entityMasks, entityMasks is reserved")
	assert(componentName ~= "COMPONENT_NONE", "component name cannot be COMPONENT_NONE, COMPONENT_NONE is reserved")
	-- type checks
	assert(type(componentName) == 'string', "first argument must be a string")
	assert(type(createComponentFunction) == 'function', "second argument must be a function")
	assert(type(resetComponentFunction) == 'function', "third argument must be a function")
	-- initialization order check
	assert(maxEntityCount ~= -1, "need to call initSystem before adding components")

	-- for each entry in system
	-- don't need to check masks because the entries should be identical
	for k, v in pairs(system) do
		-- fail if you're trying to create a component with an existing name
		assert(componentName ~= tostring(k), "component name "..componentName.." is already taken")
	end
	-- add the new component
	system[componentName] = {reset = resetComponentFunction, componentList = {}}
	-- fill it's values in the system
	for i=1,maxEntityCount do
		system[componentName].componentList[i] = createComponentFunction()
	end

	masks[componentName] = bit.lshift(1, currentMaskShift)
	currentMaskShift = currentMaskShift + 1
end

-- pass in components by string name
function createEntity(...)
	-- try to find an empty entity
	local entityIndex = ENTITY_NONE 
	for i=1,#system.entityMasks do
		if system.entityMasks[i] == masks.COMPONENT_NONE then 
			entityIndex = i
			break
		end
	end

	-- ecs is at max
	if entityIndex == ENTITY_NONE then 
		return entityIndex
	end 

	-- build your initial mask
	local resultingMask = buildBitmask(...)

	-- set your mask
	system.entityMasks[entityIndex] = resultingMask

	-- TODO: wrap this in an object so you can't accidentally use it directly
	return entityIndex
end

-- in: component name strings
function buildBitmask(...)
	local args = {...}
	local resultingMask = 0
	for i=1, #args do
		resultingMask = bit.bor(resultingMask, masks[args[i]])
	end
	return resultingMask
end

function hasComponent(entity, componentName) 
	if not isAlive(entity) then return false end
	local componentMask = masks[componentName]
	return (bit.band(system.entityMasks[entity], componentMask) == componentMask)
end

function addComponent(entity, componentName)
	-- if you don't already have it 
	if not hasComponent(entity, componentName) then 
		-- reset the component
		system[componentName].reset(system[componentName].componentList[entity])
		-- add the component to your mask
		system.entityMasks[entity] = bit.bor(system.entityMasks[entity], masks[componentName])
	end
end

function getComponent(entity, componentName)
	if not isAlive(entity) or not hasComponent(entity, componentName) then 
		return nil 
	end 
	return system[componentName].componentList[entity]
end

-- in: list of component names 
-- out: list of entities for the given mask
function getEntitiesForMask(...)
	local componentMask = buildBitmask(...)
	local entitiesForMask = {}
	for i=1,#system.entityMasks do
		if bit.band(system.entityMasks[i], componentMask) == componentMask then 
			table.insert(entitiesForMask, i)
		end
	end
	return entitiesForMask
end

function removeComponent(entity, componentName)
	system.entityMasks[entity] =  bit.band(system.entityMasks[entity], bit.bnot(masks[componentName]))
end

function destroyEntity(entity) 
	system.entityMasks[entity] = ENTITY_NONE
	return system.entityMasks[entity]
end

function isAlive(entity)
	return (system.entityMasks[entity] ~= ENTITY_NONE)
end

-- a debug function
-- iterate over each component and make sure there haven't been any variable additions
function checkSystemIntegrity()

end
