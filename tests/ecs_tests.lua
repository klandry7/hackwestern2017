
function bitwiseOpsTest() 
	-- bitwise and (1)
	local bitAnd = bit.band(3,5)
	-- bitwise or (7)
	local bitOr = bit.bor(3, 5)
	-- bitwise xor (6)
	local bitXOR = bit.bxor(3, 5)
	-- bitwise left shift (3)
	local bitLShift = bit.lshift(7, 1) 
	-- bitwise right shift (14)
	local bitRShift = bit.rshift(7, 1) 
	-- bitwise not (-8)
	local bitNot = bit.bnot(7)
end

-- reminder you can access keys by . or [] as long as the name is right
--[[local testTable = {
	itemA = "itemA"
}
testTable["itemB"] = "itemBb"
print(testTable.itemA, testTable["itemA"], testTable.itemB, testTable["itemB"])]]

function test_ecs()
	initSystem(10) 

	addComponentToSystem("componentA", function() return {intValue = 0} end, function(component) component.intValue = 0 end)
	addComponentToSystem("componentB", function() return {stringValue = "test"} end, function(component) component.stringValue = "test" end)
	addComponentToSystem("componentC", function() return {x = 0, y = 0} end, function(component) component.x = 0 component.y = 0 end)

	local entity1 = createEntity("componentA", "componentB")

	local entity2 = createEntity("componentC", "componentB")

	print()
	print("entity id: ", entity1)
	print("entity has componentA: "..tostring(hasComponent(entity1, "componentA")))
	print("entity has componentB: "..tostring(hasComponent(entity1, "componentB")))
	print("entity has componentC: "..tostring(hasComponent(entity1, "componentC")))

	print("adding componentC...")
	addComponent(entity1, "componentC")
	print("entity has componentC: "..tostring(hasComponent(entity1, "componentC")))	

	print("removing componentC...")
	removeComponent(entity1, "componentC")
	print("entity has componentC: "..tostring(hasComponent(entity1, "componentC")))	

	--[[local initialComponentA = getComponent(entity1, "componentA")
	print(initialComponentA.intValue)
	initialComponentA.intValue = 303
	local modifiedComponentA = getComponent(entity1, "componentA")
	print(modifiedComponentA.intValue)]]

	print("entity is alive: "..tostring(isAlive(entity1)))
	print("destroying entity...")
	destroyEntity(entity1) 
	print("entity is alive: "..tostring(isAlive(entity1)))
	print("entity has componentA: "..tostring(hasComponent(entity1, "componentA")))
	print("entity has componentB: "..tostring(hasComponent(entity1, "componentB")))
	print("entity has componentC: "..tostring(hasComponent(entity1, "componentC")))
end